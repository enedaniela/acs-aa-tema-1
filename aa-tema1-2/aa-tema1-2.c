/*
Ene Daniela Elena
326CC
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int **L;

/*Functie care intoarce maximul dintre doua numere*/
int max(int a, int b)
{
	if(a > b)
		return a;
	return b;
}

/*Algoritm recursiv*/
int lcs_length_recursive(char * A, char * B)
{
	if (*A == '\0' || *B == '\0') 
		return 0;
	else 
		if (*A == *B) 
			return 1 + lcs_length_recursive(A+1, B+1);
		else 
			return max(lcs_length_recursive(A+1,B), lcs_length_recursive(A,B+1));
}

/*Algoritm dinamic*/
int lcs_length_dynamic(char * A, char * B)
{
	int i,j;
	int m = strlen(A);
	int n = strlen(B);

    L = malloc((m+2) * sizeof( int *));
    /*alocare spatiu pentru matricea L*/
	for (i = 0; i <= m; i++)
	    L[i] = malloc((n+2) * sizeof(int));
	//initializarea matricei cu 0
	for (i = 0; i <= m; i++)
    {
    	for (j = 0; j <= n; j++)
    	{
    		L[i][j] = 0;

    	}
    }
    /*completarea matricei incepand din colt stanga sus*/
	for (i = 1; i <= m; i++)
	    for (j = 1; j <= n; j++)
	    {
			if (B[j-1] == A[i-1]) 
				L[i][j] = 1 + L[i-1][j-1];
				else 
					L[i][j] = max(L[i-1][j], L[i][j-1]);
	    }
	/*se returneaza lungimea LCS care este reprezentata de
	ultimul element din matrice*/
	return L[m][n];
}

//Functie ce genereaza o cifra aleator
int genRandomNumber()
{
	int i;
	return i = rand()%10+48;
}

/*Functie ce primeste ca paramtru o dimensiune si completeaza
un vector de tip char*si de dimensiunea data cu cifre aleatoare*/
char * genVector(int dim)
{
	int i;
	char * a;
	a = malloc(dim*sizeof(char));
	for(i = 0; i < dim; i++)
		a[i] = genRandomNumber();
	return a;
}

/*Functie ce verifica daca rezultatele intoarse de cei doi 
algoritmi sunt egale*/
void check(char* A,char* B)
{

	int d, r;
	printf("\n");
	printf("Stringurile sunt: %s %s\n",A,B);
	d = lcs_length_dynamic(A,B);
	r = lcs_length_recursive(A,B);
	printf("Programarea dinamica: %d\n",d);
	printf("Functia recursiva : %d\n",r);
	if(d == r)
		printf("Algoritmii intorc aceeasi valoare.\n");
	else
		printf("Algoritmii intorc rezultate diferite.\n");
}

int main()
{
	int m, n;
	clock_t start_t, end_t, total_t;
	char A[11];
	char B[11];

	/*Task 3c)*/
	/*------------------------------------------------------------*/
	printf("Task 3c)");
	char C[10] = "12mt1er5a";
	char D[8] = "42a612a";
	check(C,D);
	/*------------------------------------------------------------*/

	/*Task 3e)*/
	/*------------------------------------------------------------*/
	printf("Task 3e)");

	/*se genereaza cei doi vectori cu elemente aleatoare*/
	srand(time(NULL));
	m = rand()%10;
	n = rand()%10;
	strcpy(A,genVector(m));
	strcpy(B,genVector(n));

	/*se apeleaza functia de verificare*/
	check(A,B);

	/*se afiseaza timpii de executie pentru cei doi algoritmi*/	
	start_t = clock();
	lcs_length_dynamic(A,B);
	end_t = clock();
	total_t =(double)(end_t - start_t);
	printf("Timp de executie al algoritmului dinamic %ld\n",total_t);

	start_t = clock();
	lcs_length_recursive(A,B);
	end_t = clock();
	total_t =(double)(end_t - start_t);
	printf("Timp de executie al algoritmului recursiv %ld\n",total_t);
	/*------------------------------------------------------------*/
	return 0;
}
